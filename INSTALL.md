# Instalación dependencias

Se requiere hugo.

```
sudo apt install hugo
```

# Desarrollo


Para lanzar el servidor local:

```
hugo server -D
```

# Despliegue automático

Gitlab contacta el servidor de webhooks (https://rednegra.net/webhook), lo que lanza el siguiente script (`/home/slesage/hooks/rednegra.net.sylvainlesage.sh`):

```
#!/usr/bin/env bash

# Update workspace
rm -rf /tmp/workspace_rednegra.net.sylvainlesage
git clone --recurse-submodules https://framagit.org/severo/hugo_rednegra.net.git /tmp/workspace_rednegra.net.sylvainlesage

# Generate website
cd /tmp/workspace_rednegra.net.sylvainlesage
hugo

rm -rf /var/www/sylvainlesage/*
cp -r public/* /var/www/sylvainlesage/

rm -rf /tmp/workspace_rednegra.net.sylvainlesage
```
