---
title: "ICT assessment"
description: "I provide support for decision making, I write plans, technical proposals, and legislation projects, related to information and communication technologies, telecommunications and electronic government."
slug: "assessment"
image: "asesoramiento.jpg"
imageAlt: "credit: pxhere, CC0 Public domain"
keywords: "ICT, information and communication technologies, electronic government, Bolivia, legislation, norms, technical reports, negotiation, planning, strategy, projects"
categories:
    - "services"
date: 2018-03-13T19:26:00-04:00
draft: false
---

As Executive Director of the Agency for the Development of the Information Society in Bolivia ([ADSIB](https://adsib.gob.bo/)), and after as the chief of innovation, research and development at the Agency of Electronic Government and Information and Communication Technologies ([AGETIC](https://agetic.gob.bo/)), I participated in projects related to electronic government, administrative procedures simplification, provision of ICT services ([domain names](https://nic.bo), [digital signature](https://firmadigital.bo)), and in elaborating and negotiating norms and legislation as the [Electronic Government Plan](https://digital.gob.bo/plan-de-implementacion-de-gobierno-electronico/), the [Free Software and Open Standards Implementation Plan](https://digital.gob.bo/plan-de-implementacion-de-software-libre-y-estandares-abiertos/), the [Supreme Decree Nº2514](https://digital.gob.bo/decreto-supremo-n-2514/) that created the AGETIC, and [various technical standards](https://www.ctic.gob.bo/productos-aprobados/) approved by the Information and Communication Technologies Council of the Plurinational State of Bolivia ([CTIC-EPB](https://ctic.gob.bo)).

In the [Permanent Mission of Bolivia to the United Nations](http://boliviaenlaonu.org/) in Geneva, Switzerland, I provided support for the statements of Bolivia in themes related to science, technology and e-commerce, in the Economic and Social Council of the UN ([ECOSOC](https://www.un.org/ecosoc/en)) and in the [WTO](https://www.wto.org/spanish/tratop_s/ecom_s/ecom_s.htm).

I wrote a huge quantity of technical reports, for proposing or evaluating technological projects, related to technological infrastructure, data centers, networks, internet exchange points, telecommunication and ICT normative, systems development, public key infrastructure, domain names, codification of data registries, data analysis, among others.
