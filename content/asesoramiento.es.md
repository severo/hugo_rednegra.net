---
title: "Asesoramiento TIC"
description: "Apoyo a la toma de decisiones, redacto planes, propuestas técnicas, y proyectos de normativa, relacionados a las tecnologías de la información, las telecomunicaciones y el gobierno electrónico."
slug: "asesoramiento"
image: "asesoramiento.jpg"
imageAlt: "crédito: pxhere, CC0 Dominio público"
keywords: "TIC, tecnologías de información y comunicación, gobierno electrónico, Bolivia, normativa, informe técnico, negociación, planificación, estrategia, proyecto"
categories:
    - "servicios"
date: 2018-03-13T19:26:00-04:00
draft: false
---

Como Director Ejecutivo de la Agencia para el Desarrollo de la Sociedad de la Información en Bolivia ([ADSIB](https://adsib.gob.bo/)), y luego como Jefe de Unidad de Innovación, Investigación y Desarrollo de la Agencia de Gobierno Electrónico y Tecnologías de Información y Comunicación ([AGETIC](https://agetic.gob.bo/)), participé en proyectos de gobierno electrónico, simplificación de trámites, prestación de servicios TIC ([nombres de dominio](https://nic.bo), [firma digital](https://firmadigital.bo)), y en la elaboración y negociación de normativa como el [Plan de Gobierno Electrónico](https://digital.gob.bo/plan-de-implementacion-de-gobierno-electronico/), el [Plan de Implementación de Software Libre y Estándares Abiertos](https://digital.gob.bo/plan-de-implementacion-de-software-libre-y-estandares-abiertos/), el [Decreto Supremo Nº2514](https://digital.gob.bo/decreto-supremo-n-2514/) de creación de la AGETIC, y [varios estándares técnicos](https://www.ctic.gob.bo/productos-aprobados/) aprobados por el Consejo de Tecnologías de Información y Comunicación del Estado Plurinacional de Bolivia ([CTIC-EPB](https://ctic.gob.bo)).

En la [Misión Permanente de Bolivia ante la Naciones Unidas](http://boliviaenlaonu.org/) en Ginebra, Suiza, apoyé en las intervenciones de Bolivia sobre ciencia y tecnología, y comercio electrónico, en el Consejo Económico y Social de la ONU ([ECOSOC](https://www.un.org/ecosoc/es)) y en la [OMC](https://www.wto.org/spanish/tratop_s/ecom_s/ecom_s.htm).

Redacté una gran cantidad de informes técnicos, tanto de propuesta como de evaluación de proyectos tecnológicos, en relación con infraestructura tecnológica, centros de datos, redes, interconexión de operadores de telecomunicaciones, normativa de telecomunicaciones y TIC, desarrollo de sistemas, infraestructura de clave pública, nombres de dominio, codificación de registros, análisis de datos, entre otros.
