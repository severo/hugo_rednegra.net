---
title: "Conseil TIC"
description: "J'aide à la prise de décisions, je rédige des plans, propositions techniques, et projets de normes, en relation avec les technologies de l'information et communication, les télécommunications et le gouvernement électronique."
slug: "conseil"
image: "asesoramiento.jpg"
imageAlt: "crédit: pxhere, CC0 Domaine public"
keywords: "TIC, technologies de l'information et communication, gouvernement électronique, Bolivie, normes, rapports techniques, négociation, planification, stratégie, projets"
categories:
    - "services"
date: 2018-03-13T19:26:00-04:00
draft: false
---

En tant que directeur exécutif de l'Agence pour le Développement de la Société de l'Information en Bolivie ([ADSIB](https://adsib.gob.bo/)), et ensuite comme chef d'unité d'innovation, recherche et développement de l'Agence de Gouvernement Électronique et Technologies de l'Information et Communication ([AGETIC](https://agetic.gob.bo/)), j'ai géré et participé à de nombreux projets de gouvernement électronique, simplification de démarches, prestation de services TIC ([noms de domaine](https://nic.bo), [signature électronique](https://firmadigital.bo)), et à l'élaboration et négociation de normes comme le [Plan de Gouvernement Électronique](https://digital.gob.bo/plan-de-implementacion-de-gobierno-electronico/), le [Plan d'Implémentation de Logiciel Libre et Standards Ouverts](https://digital.gob.bo/plan-de-implementacion-de-software-libre-y-estandares-abiertos/), le [Décret Suprême Nº2514](https://digital.gob.bo/decreto-supremo-n-2514/) de création de l'AGETIC, et [plusieurs standards techniques](https://www.ctic.gob.bo/productos-aprobados/) adoptés via le Conseil de Technologies de l'Information et Communication de l'État Plurinational de Bolivie ([CTIC-EPB](https://ctic.gob.bo)).

Pour la [Mission Permanente de Bolivie auprès des Nations Unies](http://boliviaenlaonu.org/) à Genève, Suisse, j'ai aidé lors des interventions de la Bolivie sur les thèmes de la science, la technologie et le commerce électronique, lors du Conseil Économique et Social de l'ONU ([ECOSOC](https://www.un.org/ecosoc/fr)) et au sein de l'[OMC](https://www.wto.org/spanish/tratop_s/ecom_s/ecom_s.htm).

J'ai rédigé un grand nombre de rapports techniques, que ce soit pour la proposition ou l'évaluation de projets technologiques d'infrastructure, de centres de données, de réseaux, d’interconnexion d'opérateurs de télécommunications, de normes et de régulation de télécommunications et TIC, de développement logiciel, d'infrastructure de clé publique, de noms de domaine, de codification de registres de données, d'analyse de données, entre autres.
