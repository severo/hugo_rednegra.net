---
title: "Scientific computing"
description: "I provide solutions to advanced problems of simulation or optimization of industrial processes, computer vision, business intelligence, using techniques of linear algebra, statistics, genetic algorithms, mathematical morphology, neural networks and machine learning."
slug: "computing"
image: "cálculo.jpg"
imageAlt: "Neural Style of an Angry Owl, credit: Charles Durham, Flickr, Public domain"
keywords: "machine learning, business intelligence, optimization, simulation, statistics, algorithm, neural network, classification, industrial processes, free software, python"
categories:
    - "services"
date: 2018-03-13T19:26:00-04:00
draft: false
---

I realized my PhD thesis in telecommunications and signal processing, obtained at the [University of Rennes 1](https://www.univ-rennes1.fr/), France ([Learning structured dictionaries for sparse modeling of multichannel signals, 2007](https://hal.archives-ouvertes.fr/tel-00564061/)), on machine learning, using techniques of linear algebra, and applying it to the specific problem of separation of audio sources, as shown in some of my [scientific publications](https://www.researchgate.net/scientific-contributions/57113422_Sylvain_Lesage).

In the [Acsystème](http://acsysteme.com/en/) company, I realized various studies and developments related to scientific computing, simulation and optimization of industrial processes, as for example: simulation of a car suspension system, minimization of air contamination for a car motor, signal processing to detect the sense of rotation of a pressure sensor located inside the tires of a car, visualization, simulation and optimization of the electric production of hydraulic plants, automatic classification of fish species using computer vision for fishing ports, simulation and optimization of robotic filling of sardine cans, and simulation and scaling of the electric system and battery of electric bicycles.
