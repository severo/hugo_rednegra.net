---
title: "Cálculo científico"
description: "Resuelvo problemas avanzados de simulación y optimización de procesos industriales, de visión por computadora y de inteligencia de negocios, usando técnicas de álgebra lineal, estadísticas, algoritmos genéticos, morfología matemática, redes neurales y aprendizaje automático (*machine learning*)."
slug: "cálculo"
image: "cálculo.jpg"
imageAlt: "Neural Style of an Angry Owl, crédito: Charles Durham, Flickr, Dominio público"
keywords: "machine learning, aprendizaje automático, inteligencia de negocios, optimización, simulación, estadísticas, algoritmo, red neuronal, clasificación, procesos industriales, software libre, Python"
categories:
    - "servicios"
date: 2018-03-13T19:26:00-04:00
draft: false
---

Realicé mi tesis de doctorado en telecomunicaciones y procesamiento de señales, obtenido en la [Universidad de Rennes 1](https://www.univ-rennes1.fr/), Francia ([Aprendizaje de diccionarios estructurados para la modelización parsimoniosa de señales multicanales, 2007](https://hal.archives-ouvertes.fr/tel-00564061/)), sobre el aprendizaje automático, utilizando técnicas de álgebra lineal, y aplicándolo al problema específico de la separación de fuentes de sonido, como se refleja en algunas de mis [publicaciones científicas](https://www.researchgate.net/scientific-contributions/57113422_Sylvain_Lesage).

En la empresa [Acsystème](http://acsysteme.com/en/), en Francia, realicé múltiples estudios y desarrollos relacionados con el cálculo científico y la simulación y optimización de procesos industriales, como por ejemplo: simulación de sistema de amortización de un auto, minimización de contaminación atmosférica de motor, procesamiento de señales para detección de rotación en sensores de presión incorporados a neumáticos de autómovil, visualización, simulación y optimización de producción eléctrica en centrales hidraúlicas, clasificación automática de especies de peces por visión por computadora para puertos de pesca, simulación y optimización del llenado robotizado de enlatados de pescado, y simulación y dimensionamiento del sistema eléctrico de bicicletas eléctricas.
