---
title: "Calcul scientifique"
description: "Je propose des solutions aux problèmes avancés de simulation et optimisation de processus industriels, de vision par ordinateur et d'intelligence d'affaire, à l'aide de techniques d'algèbre linéaire, statistiques, algorithmes génétiques, morphologie mathématique, réseaux de neurones et apprentissage automatique (*machine learning*)."
slug: "calcul"
image: "cálculo.jpg"
imageAlt: "Neural Style of an Angry Owl, crédit: Charles Durham, Flickr, Domaine public"
keywords: "machine learning, apprentissage automatique, intelligence d'affaire, optimisation, simulation, statistiques, algorithmes, réseau de neurones, classification, processus industriels, logiciel libre, Python"
categories:
    - "services"
date: 2018-03-13T19:26:00-04:00
draft: false
---

J'ai réalisé ma thèse de doctorat en télécommunications et traitement de signal, obtenue à l'[Université de Rennes 1](https://www.univ-rennes1.fr/), France ([Apprentissage de dictionnaires structurés pour la modélisation parcimonieuse de signaux multicanaux, 2007](https://hal.archives-ouvertes.fr/tel-00564061/)), sur l'apprentissage automatique, en utilisant des techniques d'algèbre linéaire, et en l'appliquant au problème de la séparation de sources sonores, comme le montrent quelques unes de mes [publications scientifiques](https://www.researchgate.net/scientific-contributions/57113422_Sylvain_Lesage).

Pour l'entreprise [Acsystème](http://acsysteme.com/en/), j'ai réalisé de nombreuses études et développements en lien avec le calcul scientifique et la simulation et optimisation de processus industriels, comme par exemple : la simulation d'un système d'amortissement automobile, la minimisation de la pollution atmosphérique d'un moteur, le traitement de signaux pour détecter le sens de rotation d'un capteur de pression intégré aux pneus, la visualisation, simulation et optimisation de la production électrique de centrales hydrauliques, la classification automatique d'espèces de poissons par vision para ordinateur pour un port de pêche, la simulation et optimisation du remplissage de boites de conserve de sardines, ou la simulation et dimensionnement du système électrique et de la batterie d'un vélo électrique.
