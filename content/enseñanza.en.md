---
title: "Teaching"
description: "I design academic programs, and give classes, on topics like scientific computing, data analysis, geographic information systems, software development, free software, GNU/Linux servers administration, digital signal processing, machine learning, ICT politics and legislation, electronic government, digital signature, and basic cryptography."
slug: "teaching"
image: "enseñanza.jpg"
imageAlt: "Book tunnel, credit: Petr Kratochvil, CC0 Public domain"
keywords: "master, specialization, curricular design, classes, teaching, technology, ICT, computing, GIS, mathematics, maps, free software, electronic government, servers, GNU/Linux, cryptography, digital signature, Bolivia"
categories:
    - "services"
date: 2018-03-13T19:26:00-04:00
draft: false
---

I gave classes of [Matlab](https://www.mathworks.com/products/matlab.html), C, functional analysis, and computer practices, at the [University of Rennes 1](https://www.univ-rennes1.fr/), France, while working on my PhD in telecommunications and signal processing. For the [Acsystème](http://acsysteme.com/en/) company, I also realized various formation processes on Matlab, in several countries.

In Bolivia, I gave a semester class at the Bolivian Catholic University ([UCB](http://lpz.ucb.edu.bo)) of La Paz, on digital signal processing, a master module, for the Higher University of San Andrés ([UMSA](http://www.pgi.edu.bo/)) of la Paz, on geographic information systems, and a lot of interventions and [presentations](https://rednegra.net/presentaciones/) about digital signature, free software, software development, geographic information systems.
