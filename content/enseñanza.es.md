---
title: "Enseñanza"
description: "Diseño currícula e imparto cursos de posgrado, sobre materias como cálculo científico, análisis de datos, sistemas de información geográfica, desarrollo de software, software libre, administración de servidores GNU/Linux, procesamiento digital de señales, aprendizaje automático (*machine learning*), normativa y políticas TIC, gobierno electrónico, firma digital y criptografía básica."
slug: "enseñanza"
image: "enseñanza.jpg"
imageAlt: "Book tunnel, crédito: Petr Kratochvil, CC0 Dominio público"
keywords: "maestría, posgrado, diplomado, especializacíón, currícula, cursos, enseñanza, tecnología, TIC, cálculo, matemáticas, SIG, mapas, software libre, gobierno electrónico, servidores, GNU/Linux, criptografía, firma digital, Bolivia"
categories:
    - "servicios"
date: 2018-03-13T19:26:00-04:00
draft: false
---

Impartí cursos de [Matlab](https://www.mathworks.com/products/matlab.html), C, anáĺisis funcional, trabajos prácticos en informática, en la [Universidad de Rennes 1](https://www.univ-rennes1.fr/), Francia, durante la realización de mi doctorado en telecomunicaciones y procesamiento de señales. Para la empresa [Acsystème](http://acsysteme.com/en/), en Francia, también realice múltiples procesos de capacitación sobre Matlab, en varios países.

En Bolivia, dicté la materia de procesamiento digital de señales a nivel de pregrado en la Universidad Católica Boliviana ([UCB](http://lpz.ucb.edu.bo)) de La Paz, y la materia de sistemas de información geográfica en la maestría FLOSS de la Universidad Mayor de San Andrés ([UMSA](http://www.pgi.edu.bo/)) de La Paz, así como múltiples intervenciones y [presentaciones](https://rednegra.net/presentaciones/) sobre firma digital, software libre, desarrollo de software, sistemas de información geográfica.
