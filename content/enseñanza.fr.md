---
title: "Enseignement"
description: "Je donne des cours en master et élabore des programmes académiques, pour des matières comme le calcul scientifique, l'analyse de données, les systèmes d'information géographique, le développement logiciel, le logiciel libre, l'administration de serveurs GNU/Linux, le traitement numérique de signal, l'apprentissage automatique (*machine learning*), les normes et politiques TIC, le gouvernement électronique, la signature électronique et la cryptographie de base."
slug: "enseignement"
image: "enseñanza.jpg"
imageAlt: "Book tunnel, Crédit: Petr Kratochvil, CC0 Domaine public"
keywords: "master, cours, spécialisation, programme académique, enseignement, technologie, TIC, calcul, SIG, mathématiques, cartes, logiciel libre, gouvernement électronique, serveurs, GNU/Linux, cryptographie, signature électronique, Bolivie"
categories:
    - "services"
date: 2018-03-13T19:26:00-04:00
draft: false
---

J'ai donné des cours de [Matlab](https://www.mathworks.com/products/matlab.html), C, analyse fonctionnelle, travaux pratiques en informatique, à l'[Université de Rennes 1](https://www.univ-rennes1.fr/), France, au cours de mon doctorat en télécommunications et traitement de signal. Pour l'entreprise [Acsystème](http://acsysteme.com/en/), j'ai également réalisé de multiples processus de formation sur Matlab, dans plusieurs pays.

En Bolivie, j'ai donné un semestre de cours à l'Université Catholique Bolivienne ([UCB](http://lpz.ucb.edu.bo)) de La Paz, sur le traitement numérique de signal et un module du master FLOSS de l'Université Majeure de San Andrés ([UMSA](http://www.pgi.edu.bo/)) de La Paz, sur les systèmes d'information géographique en logiciel libre, ainsi que de nombreuses interventions et [présentations](https://rednegra.net/presentaciones/) sur la signature électronique, le logiciel libre, le développement logiciel et les SIG.
