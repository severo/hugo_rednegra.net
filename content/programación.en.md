---
title: "Development"
description: "I develop scripts, applications and websites, using free software."
slug: "development"
image: "desarrollo.jpg"
imageAlt: "credit: pxhere, CC0 Public domain"
keywords: "development, programming, C, C++, PHP, JavaScript, Java, Python, MySQL, PostgreSQL, Git, free software"
categories:
    - "services"
date: 2018-03-13T19:26:00-04:00
draft: false
---

I developed various websites, using the [SPIP](http://spip.net/) CMS, among others the institutional sites of the [Vice-presidency of the Plurinational State of Bolivia](https://vicepresidencia.gob.bo), of the [Bolivian Roads Administration](https://abc.gob.bo) (ABC, La Paz) and of the [Permanent Mission of Bolivia to the United Nations](http://boliviaenlaonu.org/) in Geneva, Switzerland [^1]. I administrated the servers infrastructure of these entities, using exclusively free software, principally with Debian.

I developed various tailor-made systems, among others a project tracking system and an [electoral atlas](https://geoelectoral.gob.bo)[^2] for the Vice-presidency of the Plurinational State of Bolivia, a system for monitoring the trafficability of Bolivian roads, with use of maps and geographic database, for the ABC, a documents administration system for the Permanent Mission of Bolivia to the United Nations, all of them using PHP, and MySQL or PostgreSQL.

I contributed to two libraries in C: a Linux driver for a hardware audio codec for ARM9 in the [Kerlink](https://www.kerlink.com/) company, in France, and functions for handling DVB frames (digital TV) in the dvbnet library for the [SipRadius](http://www.sipradius.com/) company, in the United States.

I developed significantly, in Java and JavaScript, for the Spatial Data Infrastructure software [geOrchestra](https://georchestra.org). In general, I contribute sporadically to open source projects, as can be seen in [GitHub](https://github.com/severo), [Framagit](https://framagit.org/severo) or [SPIP](https://zone.spip.org/trac/spip-zone/browser/).

[^1]: the three websites have been updated, the versions I developed are no more visible.
[^2]: the current version is the result of later developments.
