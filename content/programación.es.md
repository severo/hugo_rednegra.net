---
title: "Desarrollo informático"
description: "Realizo desarrollo informático y web, usando tecnologías de software libre."
slug: "programación"
image: "desarrollo.jpg"
imageAlt: "crédito: pxhere, CC0 Dominio público"
keywords: "desarrollo, programación, C, C++, PHP, JavaScript, Java, Python, MySQL, PostgreSQL, Git, software libre"
categories:
    - "servicios"
date: 2018-03-13T19:26:00-04:00
draft: false
---

Desarrollé varios sitios web, con el manejador de contenidos [SPIP](http://spip.net/), en particular los sitios institucionales de la [Vicepresidencia del Estado Plurinacional de Bolivia](https://vicepresidencia.gob.bo), de la [Administradora Boliviana de Carreteras](https://abc.gob.bo) (ABC) y de la [Misión Permanente de Bolivia ante la Naciones Unidas](http://boliviaenlaonu.org/) en Ginebra, Suiza [^1]. Realicé la administración de la infraestructura de servidores de estas mismas entidades, así como de la ADSIB, utilizando exclusivamente herramientas de software libre, con Debian principalmente.

Desarrollé varios sistemas a medida, entre los cuales se encuentra un sistema de seguimiento de proyectos y un [atlas electoral](https://geoelectoral.gob.bo)[^2] para la Vicepresidencia del Estado, un sistema de transitabilidad, con uso de mapas y bases de datos geográficas, para la ABC, un sistema de gestión documental para la Misión Permanente de Bolivia ante la Naciones Unidas, todos con PHP, y MySQL o PostgreSQL.

Desarrollé librerías en C, en particular un controlador Linux de codec audio hardware para ARM9 en la empresa [Kerlink](https://www.kerlink.com/), de Francia, y funciones de manipulación de tramas DVB (televisión digital) en la librería dvbnet para la empresa [SipRadius](http://www.sipradius.com/), de Estados Unidos.

Contribuí significativamente al desarrollo en Java y JavaScript del software de Infraestructura de Datos Espaciales [geOrchestra](https://georchestra.org). De manera general, aporto puntualmente a proyectos de código abierto, ver mis aportes en [GitHub](https://github.com/severo), [Framagit](https://framagit.org/severo) o [SPIP](https://zone.spip.org/trac/spip-zone/browser/).

[^1]: en estos tres casos, los sitios fueron actualizados, por lo que mis desarrollos ya no son visibles.
[^2]: la versión en línea es el fruto de desarrollos posteriores.
