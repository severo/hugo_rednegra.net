---
title: "Programmation"
description: "Je développe scripts, applications et sites web, à l'aide de langages et technologies de logiciel libre."
slug: "programmation"
image: "desarrollo.jpg"
imageAlt: "Crédit: pxhere, CC0 Domaine public"
keywords: "développement, programmation, C, C++, PHP, JavaScript, Java, Python, MySQL, PostgreSQL, Git, logiciel libre"
categories:
    - "services"
date: 2018-03-13T19:26:00-04:00
draft: false
---

J'ai développé plusieurs sites web, avec le CMS [SPIP](http://spip.net/), en particulier les sites institutionnels de la [Vice-présidence de l'État Plurinational de Bolivie](https://vicepresidencia.gob.bo), de l'[Administration Bolivienne des Routes](https://abc.gob.bo) (ABC, La Paz) et de la [Mission Permanente de Bolivie auprès des Nations Unies](http://boliviaenlaonu.org/) à Genève, Suisse[^1]. J'ai réalisé l'administration de l'infrastructure de serveurs de ces différentes institutions, ainsi que de l'ADSIB (La Paz, Bolivie), à l'aide exclusivement d'outils de logiciel libre, principalement avec Debian.

J'ai développé plusieurs systèmes sur mesure, entre autres un système de suivi de projets et un [atlas électoral](https://geoelectoral.gob.bo)[^2] pour la Vice-présidence de l'État Bolivien, un système de supervision de l'état des routes, avec usage de cartes et de bases de données géographiques, pour l'ABC, un système de gestion documentaire pour la Mission Permanente de Bolivie auprès des Nations Unies, tous avec PHP, et MySQL ou PostgreSQL.

J'ai développé pour des bibliothèques en C, en particulier un contrôleur Linux de codec audio matériel pour ARM9 au sein de l'entreprise [Kerlink](https://www.kerlink.com/), en France, et des fonctions de manipulation de trames DVB (télévision numérique) avec la bibliothèque dvbnet pour l'entreprise [SipRadius](http://www.sipradius.com/), aux États-Unis.

J'ai contribué significativement au développement en Java et JavaScript du logiciel d'infrastructure de données spatiales [geOrchestra](https://georchestra.org). De façon générale, j'apporte ponctuellement du code pour des projets opensource, comme on peut le voir sur [GitHub](https://github.com/severo), [Framagit](https://framagit.org/severo) ou [SPIP](https://zone.spip.org/trac/spip-zone/browser/).

[^1]: dans ces trois cas, les sites ont été mis à jour, les versions que j'ai développées ne sont plus visibles.
[^2]: la version en ligne est le fruit de développement ultérieurs.
