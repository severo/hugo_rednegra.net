---
title: "Technical writing"
description: "I write and translate (between English, French and Spanish) technical an popularization contents."
slug: "writing"
image: "redaccion.jpg"
imageAlt: "Black pencil, credit: Jess Watters, Pexels, CC0 Public domain"
keywords: "writing, translation, English, French, Spanish, technical content, popularization, blog, press"
categories:
    - "services"
date: 2018-03-13T19:26:00-04:00
draft: false
---

I wrote and translated a large number of articles about technology, geography, and ICT politics, for [Visionscarto](https://visionscarto.net/es), and the [GeoBolivia](https://test.geo.gob.bo/blog/spip.php?auteur9) and [AGETIC](https://blog.agetic.gob.bo/author/slesage) blogs.
