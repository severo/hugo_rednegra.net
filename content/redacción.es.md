---
title: "Redacción técnica"
description: "Redacto y traduzco (entre español, francés e inglés) contenidos técnicos o de vulgarización."
slug: "redacción"
image: "redaccion.jpg"
imageAlt: "Black pencil, crédito: Jess Watters, Pexels, CC0 Dominio público"
keywords: "redacción, traducción, inglés, francés, español, técnica, vulgarización, blog, prensa"
categories:
    - "servicios"
date: 2018-03-13T19:26:00-04:00
draft: false
---

Redacté y traduje una gran cantidad de artículos sobre temáticas técnicas, geográficas, o de política relacionadas con las TIC, en [Visionscarto](https://visionscarto.net/es), los blogs de [GeoBolivia](https://test.geo.gob.bo/blog/spip.php?auteur9) y [AGETIC](https://blog.agetic.gob.bo/author/slesage).
