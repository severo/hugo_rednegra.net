---
title: "Rédaction technique"
description: "Je rédige et traduis (entre l'espagnol, le français et l'anglais) des contenus techniques ou de vulgarisation."
slug: "rédaction"
image: "redaccion.jpg"
imageAlt: "Black pencil, crédit: Jess Watters, Pexels, CC0 Domaine public"
keywords: "rédaction, traduction, anglais, français, espagnol, vulgarisation, blog, presse"
categories:
    - "services"
date: 2018-03-13T19:26:00-04:00
draft: false
---

J'ai rédigé et traduit un grand nombre d'articles sur des thèmes techniques, géographiques ou de politique en relation avec les TIC, pour [Visionscarto](https://visionscarto.net/es), et les blogs de [GeoBolivia](https://test.geo.gob.bo/blog/spip.php?auteur9) et [AGETIC](https://blog.agetic.gob.bo/author/slesage).
