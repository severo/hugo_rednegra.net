---
title: "Geographic information"
description: "I design, install and administrate geographic information systems (GIS) with free software, I create maps and I analyze geographic data."
slug: "gis"
image: "sig.jpg"
imageAlt: "Municipal elections of 2015, credit: GeoElectoral, http://geoelectoral.gob.bo/#!/elecciones/2015/dpa/1"
keywords: "GIS, maps, geographic information, data, free software, GeoServer, GeoNetwork, PostGIS, python, D3, geOrchestra"
categories:
    - "services"
date: 2018-03-13T19:26:00-04:00
draft: false
---

As the technical officer of the GeoBolivia project (the Bolivian national spatial data infrastructure), along with the technical team, we published the the first version of the [geo.gob.bo](https://geo.gob.bo) portal, using technologies like  [geOrchestra](https://georchestra.org), [GeoServer](http://geoserver.org/), [GeoNetwork](https://geonetwork-opensource.org/) and [PostGIS](http://postgis.net/).

We also created the [GeoElectoral](https://geoelectoral.gob.bo) portal, we realized various studies about geographic codes and [Geohash](https://en.wikipedia.org/wiki/Geohash), and participated to the work of the Interinstitutional Committee of the Spatial Data Infrastructure of the Plurinational State of Bolivia ([IDE-EPB](https://ideepb.geo.gob.bo/)).

I actively contributed to the development of [geOrchestra](https://www.openhub.net/accounts/severolipari) for the [Camptocamp](www.camptocamp.com) company, in France, and ultimately I'm developing  [personal projects](https://github.com/severo/) related to maps and geographic information, principally with [Python](https://www.python.org/) and [D3](https://d3js.org/).
