---
title: "Información geográfica"
description: "Diseño, instalo y administro sistemas de información geográfica (SIG) con software libre, elaboro mapas y analizo datos geográficos."
slug: "sig"
image: "sig.jpg"
imageAlt: "Elecciones municipales 2015, crédito: GeoElectoral, http://geoelectoral.gob.bo/#!/elecciones/2015/dpa/1"
keywords: "SIG, mapas, información geográfica, datos, software libre, GeoServer, GeoNetwork, PostGIS, Python, D3, geOrchestra"
categories:
    - "servicios"
date: 2018-03-13T19:26:00-04:00
draft: false
---

Como responsable informático del proyecto GeoBolivia (la infraestructura de datos espaciales de Bolivia), junto al equipo técnico, publicamos la primera versión del portal [geo.gob.bo](https://geo.gob.bo), utilizando tecnologías como [geOrchestra](https://georchestra.org), [GeoServer](http://geoserver.org/), [GeoNetwork](https://geonetwork-opensource.org/) y [PostGIS](http://postgis.net/).

También elaboramos el portal [GeoElectoral](https://geoelectoral.gob.bo), realizamos estudios sobre el código [Geohash](https://en.wikipedia.org/wiki/Geohash), y participamos a los trabajos del Comité Interinstitucional de la Infraestructura de Datos Espaciales del Estado Plurinacional de Bolivia ([IDE-EPB](https://ideepb.geo.gob.bo/)).

Participe activamente en el desarrollo de [geOrchestra](https://www.openhub.net/accounts/severolipari) para la empresa [Camptocamp](www.camptocamp.com), de Francia, y últimamente desarrollo [trabajos personales](https://github.com/severo/) relacionados con información geográfica y mapas, principalmente con [Python](https://www.python.org/) y [D3](https://d3js.org/).
