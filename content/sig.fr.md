---
title: "Information géographique"
description: "Je conçois, installe et administre des systèmes d'information géographique (SIG) en logiciel libre, j'élabore des cartes et j'analyse des données géographiques."
slug: "sig"
image: "sig.jpg"
imageAlt: "Élections municipales 2015, crédit: GeoElectoral, http://geoelectoral.gob.bo/#!/elecciones/2015/dpa/1"
keywords: "SIG, cartes, information géographique, données, logiciel libre, GeoServer, GeoNetwork, PostGIS, Python, D3, geOrchestra"
categories:
    - "services"
date: 2018-03-13T19:26:00-04:00
draft: false
---

En tant que responsable informatique du projet GeoBolivia (l'infrastructure de données spatiales de Bolivie), avec l'aide de l'équipe technique, nous avons publié la première version du portail [geo.gob.bo](https://geo.gob.bo), en utilisant des technologies comme [geOrchestra](https://georchestra.org), [GeoServer](http://geoserver.org/), [GeoNetwork](https://geonetwork-opensource.org/) et [PostGIS](http://postgis.net/).

Nous avons également développé le portail [GeoElectoral](https://geoelectoral.gob.bo), réalisé des études sur le code [Geohash](https://en.wikipedia.org/wiki/Geohash), et participé aux travaux du Comité Interinstitutionnel de l'Infrastructure de Données Spatiales de l'État Plurinational de Bolivie ([IDE-EPB](https://ideepb.geo.gob.bo/)).

J'ai contribué activement au développement de [geOrchestra](https://www.openhub.net/accounts/severolipari) pour l'entreprise [Camptocamp](www.camptocamp.com), et dernièrement je développe des [projets personnels](https://github.com/severo/) liés au traitement d'information géographique et à la création de cartes, principalement avec [Python](https://www.python.org/) et [D3](https://d3js.org/).
